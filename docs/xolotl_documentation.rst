.. xolotl documentation master file, created by
   sphinx-quickstart on Wed May  9 22:07:05 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

xolotl
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installing
   introduction
   methods
   compilers
   units
   contributing
   license



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
