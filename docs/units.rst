Units
=====

``xolotl`` doesn't come with a automatic unit handling system like in ``NEURON`` or in ``Julia``, so you have to pay attention and make sure your parameters are in the right units. The following are default units for various parameters.

================================= ===========
**Variable**                      **Units**
Length of cell                    mm
Surface area of cell              mm^2
Volume of cell                    mm^3
Voltage                           mV
Specific capacitance of membrane  nF/mm^2
Specific conductance of channel   μS/mm^2
Synaptic strength                 nS
Current injected into cell        nA
Axial resistivity                 MΩ mm
calcium conductance               μM
temperature                       K
================================= ===========
