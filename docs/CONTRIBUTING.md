# Contributing to xolotl 

`xolotl` is far from feature complete, and your contributions are welcome. 

## Reporting bugs

* Is it a bug? Are you sure the bug persists after you run `transpile` and `compile`? 
* Describe what the expected behaviour is, and what the actual behaviour was

## Requesting features

* Describe what you want
* Describe why you want it

## Adding new conductances/synapses/controllers

* Look at existing conductances/synapses/controllers and use them as a guideline
* If you're making a new conductance, put them in `c++/conductances/<first_author_name>` 
* Make sure you add a reference to the paper you're getting the conductance details from in a comment at the top of the file 



