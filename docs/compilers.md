# Compiler Support

This document lists the compilers that `xolotl` has been known to work (or not). 

| OS          | Compiler | Version | Works | 
| -------     | ------     | ------- | ----------- 
| macOS 10.12.6 | clang | Apple LLVM version 9.0.0 |  ✓ |
| Ubuntu 16.04.3 LTS | g++ | 6.3 | ✓  | 
| Ubuntu 17.04 | g++ | 4.8.5 | ✓  |
| Ubuntu 17.04 | g++ | 6.3 | ❌ |
| Ubuntu 17.04 | g++ | 7.0.1 | ❌ |
| Ubuntu 17.10 | g++ | 6.4 | ❌ |
| Ubuntu 17.10 | g++ | 7.3 | ❌ |
| Ubuntu 17.10 | g++ | 4.8 | ✓ |
| Manjaro Linux | g++ | 7.3 | ✓ |

