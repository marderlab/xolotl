# Units

This document lists the units used in `xolotl` for various things. `xolotl` doesn't come with a automatic unit handling system like in `NEURON` or in `Julia`, so you have to pay attention and make sure your parameters are in the right units. 

| Variable | Units |
| ----------| ---------- |
| Surface area of cell | mm^2 |
| Volume of cell | mm^3|
| Length of cell | mm |
| Voltage | mV |
| Specific capacitance of membrane | nF/mm^2 |
| Specific conductance of channel | μS/mm^2 |
| Synaptic strength | nS |
| Current injected into cell | nA |
| Axial resistivity | MΩ mm | 
| Calcium concentration | μM |
| Temperature | K | 